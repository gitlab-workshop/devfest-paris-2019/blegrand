package com.gitlab.davinkevin.gitlabworkshop.kaamelottquote.quote

import com.gitlab.davinkevin.gitlabworkshop.kaamelottquote.persona.Persona
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Tag
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.data.repository.findByIdOrNull
import org.springframework.test.context.jdbc.Sql

@Tag("unit")
@DataJpaTest
class QuoteRepositoryTest {

    @Autowired lateinit var repository: QuoteRepository

    @Test
    fun `should save a quote`() {
        /* Given */
        val arthur = Persona(null, "Arthur", "Pendragon")
        val quote = Quote(null, arthur, "Le Graal, je sais pas où il est mais il va y rester un moment, c'est moi qui vous l'dis !")

        /* When */
        val savedQuote = repository.save(quote)
        val fetchedQuote = repository.findByIdOrNull(savedQuote.id!!)!!

        /* Then */
        assertThat(fetchedQuote.id).isNotNull()
        assertThat(fetchedQuote.body).isEqualTo("Le Graal, je sais pas où il est mais il va y rester un moment, c'est moi qui vous l'dis !")
    }

    @Test
    @Sql(scripts = ["/db/quote.findAll.sql"])
    fun `should find by id`() {
        /* Given */

        /* When */
        val quote = repository.findByIdOrNull(1) ?: throw RuntimeException("Is not found !")

        /* Then */
        assertThat(quote.body).isEqualTo("Bohort, je vous donne l'ordre de vous rendre immédiatement en Andalousie pour y rencontrer le chef wisigoth et lui transmettre le message de paix suivant :... « Coucou »...")
        assertThat(quote.author.firstName).isEqualTo("Arthur")
        assertThat(quote.author.lastName).isEqualTo("Pendragon")
    }

    @Test
    @Sql(scripts = ["/db/quote.findAll.sql"])
    fun `should find all`() {
        /* Given */

        /* When */
        val quotes = repository.findAll()

        /* Then */
        assertThat(quotes).hasSize(2)
        assertThat(quotes[0].body).isEqualTo("Bohort, je vous donne l'ordre de vous rendre immédiatement en Andalousie pour y rencontrer le chef wisigoth et lui transmettre le message de paix suivant :... « Coucou »...")
        assertThat(quotes[0].author.firstName).isEqualTo("Arthur")
        assertThat(quotes[0].author.lastName).isEqualTo("Pendragon")
        assertThat(quotes[0].author.id).isNotNull()
        assertThat(quotes[1].body).isEqualTo("Ah non, avec les jolies seulement. C’est de là que j’ai conclu que, comme y m’touchait pas, je faisais moi-même partie des grosses mochetés.")
        assertThat(quotes[1].author.firstName).isEqualTo("Guenièvre")
        assertThat(quotes[1].author.lastName).isEqualTo("De Carmélide")
        assertThat(quotes[1].author.id).isNotNull()
    }
}
